65 XOR 42 = 107
1000001
0101010
1101011 = 64 + 32 + 8 + 2 + 1 = 107

107 XOR 42 = 65
1101011
0101010
1000001 = 65

using the operator '^' allows to do this XOR operation.

One of the issue is to be sure to go through the entire length of a text. Also how to identify the interesting solutions ? I'm trying to do it first by finding the word "the"